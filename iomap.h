//iomap
#ifndef iomap_H
#define iomap_H
//****************************************************
#define LED_01 12     // the number of the pushbutton pin
#define LED_02 11     // the number of the pushbutton pin
#define LED_03 9    // the number of the pushbutton pin
#define LED_04 8     // the number of the pushbutton pin
#define LED_05 7      // the number of the LED pin
#define LED_06 6      // the number of the LED pin
#define LED_07 5      // the number of the LED pin
#define LED_08 14      // the number of the LED pin
#define LED_09 15      // the number of the LED pin
#define LED_10 16      // the number of the LED pin
#define LED_11 17      // the number of the LED pin
#define LED_12 18      // the number of the LED pin
#define LED_13 19      // the number of the LED pin

#define Latch 48     // the number of the pushbutton pin
#define Butten_01 49     // the number of the pushbutton pin
#define Butten_02 50     // the number of the pushbutton pin
#define Butten_03 51     // the number of the pushbutton pin
#define Butten_04 52     // the number of the pushbutton pin
#define Butten_05 53      // the number of the LED pin
#define Butten_06 A0      // the number of the LED pin
#define Butten_07 A1      // the number of the LED pin
#define Butten_08 A2      // the number of the LED pin
#define Butten_09 A3     // the number of the pushbutton pin
#define Butten_10 A4      // the number of the LED pin
#define Butten_11 A5      // the number of the LED pin
#define Butten_12 A6      // the number of the LED pin
#define Butten_13 A7      // the number of the LED pin

#define keypad_Out_01 22     // the number of the pushbutton pin
#define keypad_Out_02 24     // the number of the pushbutton pin
#define keypad_Out_03 26     // the number of the pushbutton pin
#define keypad_Out_04 28     // the number of the pushbutton pin
#define keypad_In_01 23      // the number of the LED pin
#define keypad_In_02 25      // the number of the LED pin
#define keypad_In_03 27      // the number of the LED pin
#define keypad_In_04 29      // the number of the LED pin

//左油門
#define thrustL_01 30 // connect blue wire here
#define thrustL_02 32 // connect green wire here
#define thrustL_03 34 // connect yellow wire here
#define thrustL_04 36 // connect violet wire here
#define thrustL_05 38 // connect orange wire here
#define thrustL_06 40 // connect brown wire here
#define thrustL_07 42 // connect white wire here
#define thrustL_08 44 // connect gtay wire here
//右油門
#define thrustR_01 31 // connect blue wire here
#define thrustR_02 33 // connect green wire here
#define thrustR_03 35 // connect yellow wire here
#define thrustR_04 37 // connect violet wire here
#define thrustR_05 39 // connect orange wire here
#define thrustR_06 41 // connect brown wire here
#define thrustR_07 43 // connect white wire here
#define thrustR_08 45 // connect gtay wire here
//舵輪
#define helm_A 46 // connect white wire here
#define helm_B 47 // connect green wire here

//************************************************
#endif


