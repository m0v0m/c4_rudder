/*************************************Arduino***********************************
* File Name          : INFRARED_WifiUDP.c
* Author             : 張智文 
* Date               : 09/30/2014
********************************************************************************/
#include "global.h"
#include "iomap.h"
#include <SPI.h> 
#include <Ethernet.h>
#include <EthernetUdp.h>
//LCD16X2
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display
//AbsoluteEncodert_A2-G-256
unsigned char Gray_codeL[8] = {0};  //TRD_NA輸出電壓為格雷碼
unsigned char Gray_codeR[8] = {0};
unsigned char Binary_codeL[8] = {0};  //二進制編碼
unsigned char Binary_codeR[8] = {0};  
long int status_valueL = 0;  //當下角度值
long int status_valueR = 0;  
long int status_REvalueL = 0;  //前一筆角度值
long int status_REvalueR = 0;

dataToHost sendData;
dataFromHost recvData;
char buf_to_net[1024];
char buf_from_net[1024];
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress localip(192, 168, 10, 118);    // arduino wifi shield default IP
IPAddress hostip(192, 168, 10, 18);     // PC端ip
unsigned int localPort = 1112;          // local port to listen on
unsigned int hostPort = 1111;           // host port to link

EthernetUDP Udp;

/*******************************************************************************
                                      副程式
********************************************************************************/
void LED_Comfigure(void);
void keypad_Comfigure(void);
void keypad_Detection(void);
void LCDScreen (char keyboard);
void thrust_Comfigure(void);
void thrust_Detection(void);
void helm_Comfigure(void);
void UDP_Send_Pocket(void);
void UDP_Recv_Pocket(void);
void dataInConfig(void);
void dtatOutConfig(void);
/*******************************************************************************
                                    參數宣告
********************************************************************************/
int sensorValue = 0;

// variables will change:
char dataNB[] = "1234567890*#CBDE";
char showNB[] = "?";
int timeNB = 0;

char LCD_NB[] = "         ";
int LCD_cursor=0;

int last_A = 0;
int last_B = 0;
long int pulses = 0;

long int looptime = 0;//test

void setup() {
  Ethernet.begin(mac,localip);
  Udp.begin(localPort);
  // put your setup code here, to run once:
  LED_Comfigure();
  keypad_Comfigure();
  thrust_Comfigure();
  helm_Comfigure();
  lcd.init();                      // initialize the lcd 
  lcd.backlight();
  
  //pinMode(13, OUTPUT);
  Serial.begin(115200); 

}

void loop() {
  // put your main code here, to run repeatedly:
 UDP_Recv_Pocket();
 dataInConfig();
 keypad_Detection();
 thrust_Detection();
 dataOutConfig();
 if (looptime > 500) //test
 {//test
 UDP_Send_Pocket();
 looptime = 0;//test
 }//test
 looptime ++;//test
 //Serial.println(pulses);
delay(10); 

}

/*******************************************************************************
  副程式名稱:LED_Comfigure
  功能說明: 1. 帶燈開關所需的腳位設定，並設置期初始值。
********************************************************************************/
void LED_Comfigure(void)
{
 pinMode(LED_01, OUTPUT);
 pinMode(LED_02, OUTPUT); 
 pinMode(LED_03, OUTPUT); 
 pinMode(LED_04, OUTPUT);
 pinMode(LED_05, OUTPUT);
 pinMode(LED_06, OUTPUT);
 pinMode(LED_07, OUTPUT);
 pinMode(LED_08, OUTPUT);
 pinMode(LED_09, OUTPUT);
 pinMode(LED_10, OUTPUT);
 pinMode(LED_11, OUTPUT);
 pinMode(LED_12, OUTPUT);
 pinMode(LED_13, OUTPUT);
 pinMode(Butten_01, INPUT);
 pinMode(Butten_02, INPUT);
 pinMode(Butten_03, INPUT);
 pinMode(Butten_04, INPUT);
 pinMode(Butten_05, INPUT);
 pinMode(Butten_06, INPUT);
 pinMode(Butten_07, INPUT);
 pinMode(Butten_08, INPUT);
 pinMode(Butten_09, INPUT);
 pinMode(Butten_10, INPUT);
 pinMode(Butten_11, INPUT);
 pinMode(Butten_12, INPUT);
 pinMode(Butten_13, INPUT); 
}//LED_Comfigure END

/*******************************************************************************
  副程式名稱:keypad_Comfigure
  功能說明: 1. 矩陣式鍵盤所需的腳位設定，並設置期初始值。
********************************************************************************/
void keypad_Comfigure(void)
{
  pinMode(keypad_Out_01, OUTPUT);
  pinMode(keypad_Out_02, OUTPUT);
  pinMode(keypad_Out_03, OUTPUT);
  pinMode(keypad_Out_04, OUTPUT);
  pinMode(keypad_In_01, INPUT);
  pinMode(keypad_In_02, INPUT);
  pinMode(keypad_In_03, INPUT);
  pinMode(keypad_In_04, INPUT);
  digitalWrite(keypad_Out_01, LOW);
  digitalWrite(keypad_Out_02, LOW);
  digitalWrite(keypad_Out_03, LOW);
  digitalWrite(keypad_Out_04, LOW);
}//keypad_Comfigure END

/*******************************************************************************
  副程式名稱:keypad_Detection
  功能說明: 1. 使用掃描偵測的方式偵測矩陣鍵盤的按壓位置。
            2. 將比對到訊號配對先前設定的符號表去對應。           
********************************************************************************/
void keypad_Detection(void)
{
    if(timeNB>3000) showNB[0] = '?';  
  for(int j=0 ;j<4;j++)
  {
    digitalWrite(2*j+22, HIGH);
    for(int i =0;i<4;i++)
    {
      if( (digitalRead(2*i+23) == HIGH) && (showNB[0] != dataNB[4*j+i]) ) 
      {
        //Serial.println(dataNB[4*j+i]); 
        showNB[0] = dataNB[4*j+i];        
        timeNB=0;
        LCDScreen(dataNB[4*j+i]);;        
      }
    }
    digitalWrite(2*j+22, LOW);
  } 
  timeNB++;
}//keypad_Detection END

/*******************************************************************************
  副程式名稱:LCDScreen
  功能說明: 1.接收矩陣式鍵盤的按壓結果，並在其顯是於LCD螢幕上。
            2.　倘若偵測到"清除"、"更正"及"輸入"等指令，LCD螢幕作同步顯是。
********************************************************************************/
void LCDScreen (char keyboard)
{
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  lcd.setCursor(0, 1);
  // print the number of seconds since reset:
  if(keyboard == 'C')
  {
    if(LCD_cursor > 0)LCD_cursor--;
    LCD_NB[LCD_cursor] = ' ';
  }
  if(keyboard == 'B')
  {
    LCD_cursor = 0;
    for(int i=0;i<9;i++) LCD_NB[i] = ' ';
  }
  if(keyboard == 'E')
  {
    LCD_cursor = 0;
    for(int i=0;i<9;i++) LCD_NB[i] = ' ';
    lcd.setCursor(0, 0);
    lcd.print("transfer success");
  }
  if(keyboard != 'C' && keyboard != 'B' && keyboard != 'E')
  {  
    LCD_NB[LCD_cursor] = keyboard;
    LCD_cursor++;
  }
  lcd.print(LCD_NB);
}//LCDScreen END

/*******************************************************************************
  副程式名稱:thrust_Comfigure
  功能說明: 1. 油門手柄所需的腳位設定，並設置期初始值。　
********************************************************************************/
void thrust_Comfigure(void)
{
  pinMode(thrustL_01, INPUT);
  pinMode(thrustL_02, INPUT);
  pinMode(thrustL_03, INPUT);
  pinMode(thrustL_04, INPUT);
  pinMode(thrustL_05, INPUT);
  pinMode(thrustL_06, INPUT);
  pinMode(thrustL_07, INPUT);
  pinMode(thrustL_08, INPUT);
  pinMode(thrustR_01, INPUT);
  pinMode(thrustR_02, INPUT);
  pinMode(thrustR_03, INPUT);
  pinMode(thrustR_04, INPUT);
  pinMode(thrustR_05, INPUT);
  pinMode(thrustR_06, INPUT);
  pinMode(thrustR_07, INPUT);
  pinMode(thrustR_08, INPUT);
  digitalWrite(thrustL_01, HIGH);
  digitalWrite(thrustL_02, HIGH);
  digitalWrite(thrustL_03, HIGH);
  digitalWrite(thrustL_04, HIGH);
  digitalWrite(thrustL_05, HIGH);
  digitalWrite(thrustL_06, HIGH);
  digitalWrite(thrustL_07, HIGH);
  digitalWrite(thrustL_08, HIGH);
  digitalWrite(thrustR_01, HIGH);
  digitalWrite(thrustR_02, HIGH);
  digitalWrite(thrustR_03, HIGH);
  digitalWrite(thrustR_04, HIGH);
  digitalWrite(thrustR_05, HIGH);
  digitalWrite(thrustR_06, HIGH);
  digitalWrite(thrustR_07, HIGH);
  digitalWrite(thrustR_08, HIGH);  
}//thrust_Comfigure END

/*******************************************************************************
  副程式名稱:thrust_Detection
  功能說明: 1. 讀取絕對型編碼器8bit的電壓訊號。
            2. 將編碼器慣用的格雷碼轉換成二進制的資料型態。           
            3. 同時讀取左右油門兩顆絕對型編碼器的數值。
********************************************************************************/
void thrust_Detection(void)
{
  status_valueL = 0;
  status_valueR = 0;
  for(int i=0;i<8;i++)  Gray_codeL[i] = ( digitalRead(thrustL_01+(2*i)) == HIGH) ? 0 : 1;  //NPN故HIGH取0
  for(int i=0;i<8;i++)  Gray_codeR[i] = ( digitalRead(thrustR_01+(2*i)) == HIGH) ? 0 : 1;
  
  //格雷碼轉二進制編碼，詳情可看https://tw.answers.yahoo.com/question/index?qid=20051123000014KK01284
   Binary_codeL[7] = Gray_codeL[7];
   Binary_codeR[7] = Gray_codeR[7];
   for (int i = 7;i > 0;i--)  Binary_codeL[i-1] =  Binary_codeL[i] ^ Gray_codeL[i-1];
   for (int i = 7;i > 0;i--)  Binary_codeR[i-1] =  Binary_codeR[i] ^ Gray_codeR[i-1];
  
  //將二進制轉換成十進制角度值
   for(int i=0;i<8;i++) status_valueL += Binary_codeL[i]*pow(2,i);
   for(int i=0;i<8;i++) status_valueR += Binary_codeR[i]*pow(2,i);
   
   //顯示當下改變後的角度值
  if(status_REvalueL != status_valueL)
  {
    status_REvalueL = status_valueL;
    Serial.print("status_valueL:  ");
    Serial.println(status_valueL);
//    Serial.print("Gray_codeL:    "); 
//    for(int i=7;i>-1;i--)  Serial.print(Gray_codeL[i]);
//    Serial.println(" ");
//    Serial.print("Binary_codeL:  ");
//    for(int i=7;i>-1;i--)  Serial.print(Binary_codeL[i]);
//    Serial.println(" ");
    Serial.println(" ");
  }
  if(status_REvalueR != status_valueR)
  {
    status_REvalueR = status_valueR;
    Serial.print("status_valueR:  ");
    Serial.println(status_valueR);
//    Serial.print("Gray_codeR:    "); 
//    for(int i=7;i>-1;i--)  Serial.print(Gray_codeR[i]);
//    Serial.println(" ");
//    Serial.print("Binary_codeR:  ");
//    for(int i=7;i>-1;i--)  Serial.print(Binary_codeR[i]);
//    Serial.println(" ");
    Serial.println(" ");
  }
}//thrust_Detection END

/*******************************************************************************
  副程式名稱:helm_Comfigure
  功能說明: 1. 透過外部中斷來執行相對型編碼器所傳送的電壓訊號。
            2. 並將A相及B相變化轉換成數值。           
********************************************************************************/
void helm_Comfigure(void)
{
  pinMode(helm_A, INPUT);
  digitalWrite(helm_A, HIGH); // enables pull-up resistor
  pinMode(helm_B, INPUT);
  digitalWrite(helm_B, HIGH); // enables pull-up resistor  

  last_A = digitalRead(helm_A);
  last_B = digitalRead(helm_B);

  attachInterrupt(helm_A, encoderPinChange_A, CHANGE); // pin 2
  attachInterrupt(helm_B, encoderPinChange_B, CHANGE); // pin 3
  pulses = 0;  
}
void encoderPinChange_A()
{
  last_A = digitalRead(helm_A) == HIGH;
  pulses += (last_A != last_B) ? +1 : -1;
}
void encoderPinChange_B()
{
  last_B = digitalRead(helm_B) == HIGH;
  pulses += (last_A == last_B) ? +1 : -1;
}//helm_Comfigure END

/*******************************************************************************
  副程式名稱:UDP_Send_Pocket
  功能說明: 1. 設定傳送目的之IP位址及其PORT連接阜
            2. 將待傳送之結構變數寫入傳送的封包暫存空間中，並透過UDP傳送封包
********************************************************************************/
  void UDP_Send_Pocket()
  {
    Udp.beginPacket(hostip, hostPort);
    ZeroMemory(buf_to_net, sizeof(buf_to_net));
    CopyMemory(buf_to_net, &sendData, sizeof(sendData));
    Udp.write(buf_to_net,sizeof(sendData));
    Udp.endPacket();
  } //UDP_Send_Pocket End
 
/*******************************************************************************
  副程式名稱:UDP_Recv_Pocket
  功能說明: 1. 接收UDP封包，並將其儲存至對應的結構變數中。
********************************************************************************/
  void UDP_Recv_Pocket()
  {
    int packetSize = Udp.parsePacket();
    Serial.print("modeLed01: ");
    Serial.println(recvData.modeLed01);
    if (packetSize)
    {
      Udp.read(buf_to_net,sizeof(buf_to_net));
      RtlCopyMemory( &(recvData), &(buf_to_net), sizeof(recvData) );
      Serial.print("modeLed01: ");
      Serial.println(recvData.modeLed01);
      Serial.print("modeLed02: ");
      Serial.println(recvData.modeLed02);
      Serial.print("modeLed03: ");
      Serial.println(recvData.modeLed03);
      Serial.print("modeLed04: ");
      Serial.println(recvData.modeLed04);
      Serial.print("LedCW: ");
      Serial.println(recvData.LedCW);
      Serial.print("LedCCW: ");
      Serial.println(recvData.LedCCW);
      Serial.print("typeLed01: ");   
      Serial.println(recvData.typeLed01);
      Serial.print("typeLed02: ");
      Serial.println(recvData.typeLed02);
      Serial.print("rudderLedL: ");
      Serial.println(recvData.rudderLedL);
      Serial.print("rudderLedC: ");
      Serial.println(recvData.rudderLedC);
      Serial.print("rudderLedR: ");
      Serial.println(recvData.rudderLedR);
      Serial.print("engineLedL: ");
      Serial.println(recvData.engineLedL);
      Serial.print("engineLedR: ");
      Serial.println(recvData.engineLedR);
      Serial.print("ciodrediop2: ");
      Serial.println(recvData.ciodrediop2);
    }       
  } //UDP_Recv_Pocket End
  
/*******************************************************************************
  副程式名稱:dataInConfig
  功能說明: 1. 將接收到的訊息，轉換成對應腳位的電壓訊號。
********************************************************************************/
void dataInConfig()
{
  recvData.modeLed01 != '0'  ? digitalWrite(LED_02 , LOW) : digitalWrite(LED_02 , HIGH);
  recvData.modeLed02 != '0'  ? digitalWrite(LED_01 , LOW) : digitalWrite(LED_01 , HIGH);
  recvData.modeLed03 != '0'  ? digitalWrite(LED_04 , LOW) : digitalWrite(LED_04 , HIGH);
  recvData.modeLed04 != '0'  ? digitalWrite(LED_03 , LOW) : digitalWrite(LED_03 , HIGH);
  recvData.LedCCW != '0'     ? digitalWrite(LED_05 , LOW) : digitalWrite(LED_05 , HIGH);
  recvData.LedCW != '0'      ? digitalWrite(LED_06 , LOW) : digitalWrite(LED_06 , HIGH);
  recvData.typeLed02 != '0'  ? digitalWrite(LED_07 , LOW) : digitalWrite(LED_07 , HIGH);
  recvData.typeLed01 != '0'  ? digitalWrite(LED_08 , LOW) : digitalWrite(LED_08 , HIGH);
  recvData.rudderLedR != '0' ? digitalWrite(LED_09 , LOW) : digitalWrite(LED_09 , HIGH);
  recvData.rudderLedC != '0' ? digitalWrite(LED_10 , LOW) : digitalWrite(LED_10 , HIGH);
  recvData.rudderLedL != '0' ? digitalWrite(LED_11 , LOW) : digitalWrite(LED_11 , HIGH);
  recvData.engineLedR != '0' ? digitalWrite(LED_12 , LOW) : digitalWrite(LED_12 , HIGH);
  recvData.engineLedL != '0' ? digitalWrite(LED_13 , LOW) : digitalWrite(LED_13 , HIGH);
}//dataInConfig END

/*******************************************************************************
  副程式名稱:dataOutConfig
  功能說明: 1. 接收DI訊號，並將其電位狀態儲存至對應的軟體變數中。
            2. 將一些轉換電壓訊號而求得的計算結果儲存到對應的軟體變數中。
********************************************************************************/
void dataOutConfig()
{
  sendData.thrustLock    = (digitalRead(Latch)==HIGH)     ? '1': '0';  
  sendData.modeBtn02     = (digitalRead(Butten_01)==HIGH) ? '1': '0';
  sendData.modeBtn01     = (digitalRead(Butten_02)==HIGH) ? '1': '0'; 
  sendData.modeBtn04     = (digitalRead(Butten_03)==HIGH) ? '1': '0'; 
  sendData.modeBtn03     = (digitalRead(Butten_04)==HIGH) ? '1': '0'; 
  sendData.turnCCW       = (digitalRead(Butten_05)==HIGH) ? '1': '0'; 
  sendData.turnCW        = (digitalRead(Butten_06)==HIGH) ? '1': '0'; 
  sendData.typeBtn02     = (digitalRead(Butten_07)==HIGH) ? '1': '0'; 
  sendData.typeBtn01     = (digitalRead(Butten_08)==HIGH) ? '1': '0'; 
  sendData.rudderBtnR    = (digitalRead(Butten_09)==HIGH) ? '1': '0'; 
  sendData.rudderBtnC    = (digitalRead(Butten_10)==HIGH) ? '1': '0'; 
  sendData.rudderBtnL    = (digitalRead(Butten_11)==HIGH) ? '1': '0'; 
  sendData.engineStartR  = (digitalRead(Butten_12)==HIGH) ? '1': '0'; 
  sendData.engineStartL  = (digitalRead(Butten_13)==HIGH) ? '1': '0';
  sendData.thrustL       = status_valueL;
  sendData.thrustR       = status_valueR;
  sendData.helm          = pulses;
  sendData.ctrlDirect    = atof(LCD_NB);
}// dataOutConfig END
