// BYTE type definition
#ifndef global_H
#define global_H
//****************************************************
#define RtlCopyMemory(Destination,Source,Length) memcpy((Destination),(Source),(Length))
#define RtlZeroMemory(Destination,Length) memset((Destination),0,(Length))
#define CopyMemory RtlCopyMemory
#define ZeroMemory RtlZeroMemory

typedef struct{ 
  char modeBtn01;
  char modeBtn02;
  char modeBtn03;
  char modeBtn04;
  char turnCW;
  char turnCCW;
  char typeBtn01;
  char typeBtn02;
  char rudderBtnL;  
  char rudderBtnC;
  char rudderBtnR;
  char engineStartL;
  char engineStartR;
  char thrustLock;
  char NC01;
  char NC02;
  long int thrustL;
  long int thrustR;
  long int helm;
  float ctrlDirect;   
}dataToHost;

typedef struct{
  char modeLed01;
  char modeLed02;
  char modeLed03;
  char modeLed04;
  char LedCW;
  char LedCCW;
  char typeLed01;
  char typeLed02;
  char rudderLedL;
  char rudderLedC;
  char rudderLedR;
  char engineLedL;
  char engineLedR;
  char ciodrediop2;
}dataFromHost;

//************************************************
#endif


